from django.urls import path

from . import views

app_name = 'homepage' 

urlpatterns = [
    path('', views.tambahEmail, name='home'),
    path('notif/', views.notifEmail, name='notifEmail'),
    path('dataemail/', views.dataemail, name='dataemail'),
    path('dataemail/kirimemail/', views.kirimemail, name='kirimemail'),
    path('dataemail/tampilanemail/',views.tampilan,name='tampilanemail')     
]
