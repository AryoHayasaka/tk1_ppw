from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import tambahEmail,notifEmail, dataemail,kirimemail,tampilan
from .models import Email,KirimEmail
from .forms import emailForm

class TestEmail(TestCase):

    # landing
    def test_url_landing_tambah(self):
        response = Client().get('')
        self.assertEquals(200, response.status_code)

    def test_template_landing(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'landing.html')

    def test_view_landing(self):
        response = self.client.post('', data= {'email' : 'test@ppw.com'})
        self.assertEquals(Email.objects.count(), 1)

    def test_model_landing(self):
        new_activity = Email.objects.create(email='test@ppw.com')
        count = Email.objects.all().count()
        self.assertEqual(count, 1)
        self.assertEqual(str(new_activity),'test@ppw.com')

    # notif
    def test_url_notif(self):
        response = Client().get('/notif/')
        self.assertEquals(200, response.status_code)

    def test_template_notif(self):
        response = Client().get('/notif/')
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'notif.html')

    # data email
    def test_url_dataemail(self):
        response = Client().get('/dataemail/')
        self.assertEquals(200, response.status_code)

    def test_template_dataemail(self):
        response = Client().get('/dataemail/')
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'dataemail.html')
    
    # kirim email
    def test_url_kirimemail(self):
        response = Client().get('/dataemail/kirimemail/')
        self.assertEquals(200, response.status_code)

    def test_template_kirimemail(self):
        response = Client().get('/dataemail/kirimemail/')
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'kirimemail.html')

    def test_model_kirimemail(self):
        new_activity = KirimEmail.objects.create(subjek = 'testsubjek',judul= 'testjudul',deskripsi ='testdeskripsi')
        count = KirimEmail.objects.all().count()
        self.assertEqual(count, 1)
        self.assertEqual(str(new_activity),'testjudul')
    
    def test_view_kirimemail(self):
        response = self.client.post('/dataemail/kirimemail/', data= {'subjek' : 'testsubjek','judul' : 'testjudul','deskripsi' : 'testdeskripsi'})
        self.assertEquals(KirimEmail.objects.count(), 1)

    # tampilan email
    def test_url_tampilanemail(self):
        response = Client().get('/dataemail/tampilanemail/')
        self.assertEquals(200, response.status_code)

    def test_template_tampilanemail(self):
        response = Client().get('/dataemail/tampilanemail/')
        self.assertTemplateUsed(response, 'base.html')
        self.assertTemplateUsed(response, 'tampilanemail.html')


    


