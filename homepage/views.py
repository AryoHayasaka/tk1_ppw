from django.shortcuts import render, redirect
from .forms import emailForm,kirimForm
from .models import Email,KirimEmail
from django.core.mail import send_mail, EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.conf import settings
from django.contrib import messages



# Create your views here

def tambahEmail(request):
    form = emailForm(request.POST or None)
    if form.is_valid():
        form.save()
        emailnya = form.cleaned_data['email']
        form = emailForm()
        html_content = render_to_string('emailsambutan.html')
        text_content = strip_tags(html_content)
        email = EmailMultiAlternatives(
    'Terima Kasih telah mendaftarkan email !', text_content, settings.EMAIL_HOST_USER, [emailnya]
)
        email.attach_alternative(html_content,"text/html")
        email.send()
        return redirect('homepage:notifEmail')
    context = {
        'form' : form
    }
    return render(request, 'landing.html', context)

def notifEmail(request):
    return render(request, 'notif.html')

def dataemail(request):
    data = Email.objects.all()
    context = {
     'data' : data
    }
    return render(request, 'dataemail.html', context)

def kirimemail(request):
    form = kirimForm(request.POST or None)
    # data = Email.objects.values('email')
    data = Email.objects.all()
    if form.is_valid():
        form.save()
    if request.method == "POST":
        subjek  = request.POST.get('subjek')
        judul  = request.POST.get('judul')
        deskripsi = request.POST.get('deskripsi')
        html_content = render_to_string('emailcustom.html',{'subjek' :subjek,'judul':judul,'deskripsi':deskripsi})
        text_content = strip_tags(html_content)
        email = EmailMultiAlternatives(
            subjek, text_content, settings.EMAIL_HOST_USER, data
        )

        email.attach_alternative(html_content,"text/html")
        email.send()
        messages.success(request,"Email telah terkirim !")
    context = {
     'form' : form
    }
    return render(request, 'kirimemail.html', context)

def tampilan(request):
    isiemail = KirimEmail.objects.all()
    context = {
    'isiemail' : isiemail
    }
    return render(request, 'tampilanemail.html', context)