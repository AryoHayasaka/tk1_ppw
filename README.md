# TK1_ppw

[![pipeline status](https://gitlab.com/AryoHayasaka/tk1_ppw/badges/master/pipeline.svg)](https://gitlab.com/AryoHayasaka/tk1_ppw/commits/master)

Anggota Kelompok:
- Aryo Wirapradana - 1906298784
- Hilmy Akbar Nugraha - 1906293101
- Hasna Nadifah - 1906293096
- Ayesha Thalia K.P - 1906298790

**Covidstudy**
link: https://tk1-covidstudy.herokuapp.com/

Website Covidstudy ini dimaksudkan sebagai suatu database besar penelitian-penelitian ilmiah yang berhubungan dengan
pandemi Covid-19. Penelitian ini dapat yang berupa penelitian atas virus itu sendiri, maupun penelitian atas dampaknya
ke masyarakat. Website ini diperuntukkan bagi pelajar, pengajar, maupun peneliti di masa depan yang berniat untuk
mempelajari tentang pandemi covid-19. Semua karya yang terdapat di covidstudy dapat diakses dengan gratis oleh siapapun.
Selain itu, covidstudy juga akan menampilkan informasi-informasi terbaru tentang kegiatan yang melibatkan covidstudy,
baik sebagai penyelenggara langsung maupun sekedar partner. User dapat upload studi baru dengan memberi nama studi,
deskripsi studi, dan link ke file studi. Selain itu, user juga dapat mendaftarkan event baru dengan memberi informasi
tentang event sesuai dengan yang diminta oleh form. Pada bagian customer support, selain keluhan, user juga dapat memberi 
penyemangat kepada user lain lewat sebuah form. Penyemangat ini akan ditampilkan secara publik pada website.

Daftar Fitur:
- Homepage dan Subscription
- Search ,Show, Add Event
- Search, Show, Add Catalog
- Customer Support dan Penyemangat


