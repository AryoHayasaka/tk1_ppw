from django.test import TestCase, Client
from django.urls import resolve

from .models import Semangat, Keluhan
from . import views

# Create your tests here.
class UnitTestForCustomer(TestCase):
    def test_response_page(self):
        response = Client().get('/support/')
        self.assertEqual(response.status_code, 200)

    def test_template_used(self):
        response = Client().get('/support/')
        self.assertTemplateUsed(response, 'customer.html')

    def test_func_page(self):
        found = resolve('/support/')
        self.assertEqual(found.func, views.customer)
    
    def test_template_kumpulan_semangat(self):
        response = Client().get('/support/kumpulan-semangat/')
        self.assertTemplateUsed(response, 'kumpulan-semangat.html')
        #pass
    
    def test_template_berhasil(self):
        response = Client().get('/support/berhasil/')
        self.assertTemplateUsed(response, 'berhasil.html')
        #pass

    def test_model_semangat(self):
        Semangat.objects.create(nama='Dobleh')
        semangat = Semangat.objects.get(nama='Dobleh')
        self.assertEqual(str(semangat), 'Dobleh')
    
    def test_model_keluhan(self):
        Keluhan.objects.create(pesan='Pesan')
        keluhan = Keluhan.objects.get(pesan='Pesan')
        self.assertEqual(str(keluhan), 'Pesan')
    
    def test_hasil_semangat(self):
        response = Client().post('/support/kumpulan-semangat/', {'nama': 'Dobleh', 'pesan': 'Pesan'})
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Dobleh", html_kembalian)
        self.assertIn("Pesan", html_kembalian)

    def test_save_keluhan_a_POST_request(self):
        Client().post('/support/berhasil/', data={'pesan': 'Pesan'})
        jumlah = Keluhan.objects.filter(pesan='Pesan').count()
        self.assertEqual(jumlah, 1)

    def test_semangat_form_invalid(self):
        Client().post('/support/kumpulan-semangat', data={})
        jumlah = Semangat.objects.filter(nama='Unit Test').count()
        self.assertEqual(jumlah, 0)

    def test_keluhan_form_invalid(self):
        Client().post('/support/berhasil', data={})
        jumlah = Keluhan.objects.filter(pesan='Test').count()
        self.assertEqual(jumlah, 0)