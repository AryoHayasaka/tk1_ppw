from django.contrib import admin

# Register your models here.

from .models import Semangat, Keluhan

admin.site.register(Semangat)
admin.site.register(Keluhan)
