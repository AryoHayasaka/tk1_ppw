from django import forms

class semangatform(forms.Form):
    error_messages = {
        'required': 'This field is required.'
    }

    nama = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control','placeholder' : "Ketik nama Anda. Boleh ketik 'Anonymous'."}), label='Nama ', 
            max_length=50, required=True)
    pesan = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control','placeholder' : "Ketik pesan"}), label='Pesan ',
            max_length=100, required=True)
class keluhanform(forms.Form):
    error_messages = {
        'required': 'This field is required.'
    }
    attrs = {
        'class': 'form-control',
        'placeholder' : "Ketik pesan Anda di sini"
    }
    pesan = forms.CharField(widget=forms.Textarea(attrs=attrs), label='Pesan ',  max_length=100, required=True)