from django.db import models

# Create your models here.
class Semangat(models.Model):
    nama = models.CharField(max_length = 50)
    pesan = models.CharField(max_length = 100)

    def __str__(self):
        return self.nama

class Keluhan(models.Model):
    pesan = models.CharField(max_length = 100)

    def __str__(self):
        return self.pesan