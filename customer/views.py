from django.http import HttpResponse
from django.shortcuts import render, redirect

from .forms import semangatform, keluhanform
from .models import Semangat, Keluhan

# Create your views here.
def customer(request):
    form_semangat = semangatform()
    form_keluhan = keluhanform()
    response = {
        'form_semangat': form_semangat,
        'form_keluhan' : form_keluhan,
    }
    return render(request, "customer.html", response)

def post_semangat(request):
    if request.method == 'POST':
        Semangat.objects.create(nama=request.POST['nama'], pesan=request.POST['pesan'])

    daftar_semangat = Semangat.objects.all()
    print(daftar_semangat)
    response = {
        'data' : daftar_semangat
    }
    return render(request, 'kumpulan-semangat.html', response)

def post_keluhan(request):
    form = keluhanform(request.POST or None, request.FILES or None)
    response_data = {}
    if form.is_valid():
        response_data['pesan'] = request.POST['pesan']

        data_keluhan = Keluhan(pesan=response_data['pesan'])
        data_keluhan.save()
    return render(request, 'berhasil.html', response_data)