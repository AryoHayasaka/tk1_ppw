from django.test import TestCase
from django.test import Client
from datetime import date, time
from .models import Event

class TestEvent(TestCase):
    def test_url_cari(self):
        response = Client().get('/event/cari/')
        self.assertEqual(response.status_code,200)

    def test_template_cari(self):
        response = Client().get('/event/cari/')
        self.assertTemplateUsed(response, 'event/cari.html')

    def test_url_tambah(self):
        response = Client().get('/event/tambah/')
        self.assertEqual(response.status_code,200)

    def test_template_tambah(self):
        response = Client().get('/event/tambah/')
        self.assertTemplateUsed(response, 'event/tambah.html')

    def test_check_cari_page_have_form(self):
        response = Client().get('/event/cari/')
        content = response.content.decode('utf8')
        self.assertIn('<form', content)

    def test_check_tambah_page_have_form(self):
        response = Client().get('/event/tambah/')
        content = response.content.decode('utf8')
        self.assertIn('<form', content)

    def test_model_create_event(self):
        Event.objects.create(
            Jenis_Event='webinar', 
            Nama_Event = 'event_test',
            Penyelenggara = 'Kominfo',
            Tanggal_Event = date(2020, 11, 11),
            Waktu_Event = time(16, 00),
            Link_Pendaftaran = 'http://bkd.cilacapkab.go.id/p/562/webinar--seminar-online-di-masa-pandemi-covid-19'
        )
        total = Event.objects.all().count()
        self.assertEqual(total,1)

    def test_model_save_POST_request(self):
        response = Client().post('/event/tambah/', data={
            'Jenis_Event' : 'seminar', 
            'Nama_Event' : 'kumps',
            'Penyelenggara' : 'SMAN A',
            'Tanggal_Event' : date(2020, 11, 21),
            'Waktu_Event' : time(17, 00),
            'Link_Pendaftaran' : 'http://bkd.cilacapkab.go.id/p/562/webinar--seminar-online-di-masa-pandemi-covid-19'
            })
        total = Event.objects.filter(
            Jenis_Event = 'seminar', 
            Nama_Event = 'kumps',
            Penyelenggara = 'SMAN A',
            Tanggal_Event = date(2020, 11, 21),
            Waktu_Event = time(17, 00),
            Link_Pendaftaran = 'http://bkd.cilacapkab.go.id/p/562/webinar--seminar-online-di-masa-pandemi-covid-19'
        ).count()
        self.assertEqual(total,1)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/event/cari/')