from django.db import models

# Create your models here.
class Event(models.Model):
    Jenis_Event = models.CharField(max_length=100)
    Nama_Event = models.TextField(max_length=300)
    Penyelenggara = models.CharField(max_length=100)
    Tanggal_Event = models.DateField(max_length=100)
    Waktu_Event = models.TimeField(max_length=100)
    Link_Pendaftaran = models.URLField(max_length=1000, blank=False)

def __str__(self):
    return "{}".format(self.Nama_Event)