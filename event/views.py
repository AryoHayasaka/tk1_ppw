from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.db.models import Q
from django.contrib import messages 

# Create your views here.
from .models import Event
from .forms import eventForm

def index(request):
    if request.method == 'POST':
        srch = request.POST['srh']

        if srch:
            match = Event.objects.filter(Q(Nama_Event__icontains=srch) |
                                             Q(Jenis_Event__icontains=srch) |
                                             Q(Penyelenggara__icontains=srch) |
                                             Q(Tanggal_Event__icontains=srch)
                                             )
            if match:
                return render(request, 'event/cari.html', {'sr':match, 'nama':srch})
            else:
                messages.error(request, 'Hasil pencarian event tidak ditemukan, silahkan coba lagi')
        else:
            return HttpResponseRedirect('/event/cari/')

    return render(request, 'event/cari.html')

def tambah(request):
    event_form = eventForm(request.POST)
    if request.method == 'POST':
        if event_form.is_valid():
            event_form.save()
            return HttpResponseRedirect("/event/cari/")

    context = {
        'event_form' : event_form,
    }
    return render(request, 'event/tambah.html', context)