from django import forms
from .models import Study

class StudyForm(forms.ModelForm):
    class Meta:
        model = Study
        fields = ('namaStudi', 'tipeStudi', 'tanggalPublikasi', 'author', 'abstract', 'link')
        labels = {
            'namaStudi' : 'Nama Studi',
            'tipeStudi' : 'Tipe Studi',
            'tanggalPublikasi' : 'Tanggal Publikasi',
            'author' : 'Author',
            'abstract' : 'Abstrak',
            'link' : 'Link'
        }
        widgets = {
            'tanggalPublikasi' : forms.DateInput(attrs={'type':'date'}),
            'abstract' : forms.Textarea(attrs={'id':'abstract', 'placeholder':'Ketik abstrak dari studi'}),
            'namaStudi' : forms.TextInput(attrs={'placeholder':'Ketik nama studi'}),
            'author' : forms.TextInput(attrs={'placeholder':'Ketik nama penulis'}),
            'link' : forms.TextInput(attrs={'placeholder':'Ketik link pdf studi'})
        }
