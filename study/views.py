from django.shortcuts import render
from django.db.models import Q
from .forms import *
from .models import Study

# Create your views here.
def studyAddView(request):
    context = {}
    form = StudyForm(request.POST or None)
    context['form'] = form
    if request.method == 'POST' and form.is_valid():
        form = StudyForm(request.POST)
        form.save()
        context['success'] = True
    return render(request, 'study_add.html', context)

def studySearchView(request):
    context = {}
    query = request.GET.get('search', False)
    context['query'] = query
    if query:
        cari = Study.objects.filter(Q(namaStudi__icontains=query) |
                                       Q(tipeStudi__icontains=query) |
                                       Q(author__icontains=query) )
        if cari:
            context['hasil'] = cari
        else:
            context['hasil'] = 'Tidak ditemukan'
        
    return render(request, 'study_search.html', context)
