from django.test import TestCase, Client
from .models import Study
from .forms import StudyForm
from datetime import date

# Create your tests here.
class StudyTest(TestCase):
    def test_study_url_exist(self):
        response = Client().get('/studi/tambah/')
        self.assertEqual(200, response.status_code)

    def test_study_correct_template(self):
        response = Client().get('/studi/tambah/')
        self.assertTemplateUsed(response, 'study_add.html')

    def test_study_search_correct_template(self):
        response = Client().get('/studi/cari/')
        self.assertTemplateUsed(response, 'study_search.html')

    def test_study_model(self):
        Study.objects.create(namaStudi = 'lol', tipeStudi = 'Sosial', tanggalPublikasi = date(2020, 11, 11), author = 'hayasaka', abstract = 'lolololol', link = 'lol.com')
        cnt = Study.objects.all().count()
        self.assertEqual(1, cnt)

    def test_study_add_form(self):
        form = StudyForm(data={'namaStudi' : 'lol', 'tipeStudi' : 'Sosial', 'tanggalPublikasi' : date(2020, 11, 11), 'author' : 'hayasaka', 'abstract' : 'lolololol', 'link' : 'lol.com'})
        form.save()
        cnt = Study.objects.all().count()
        self.assertEqual(1, cnt)

    def test_study_search_get(self):
        Study.objects.create(namaStudi = 'lol', tipeStudi = 'Sosial', tanggalPublikasi = date(2020, 11, 11), author = 'hayasaka', abstract = 'lolololol', link = 'lol.com')
        response = Client().get('/studi/cari/', {'search':'lol'})
        html_response = response.content.decode('utf8')
        self.assertIn('lol', html_response)
        self.assertIn('Sosial', html_response)
        self.assertIn('hayasaka', html_response)
        self.assertIn('lolololol', html_response)

    def test_study_search_get_notFound(self):
        Study.objects.create(namaStudi = 'lol', tipeStudi = 'Sosial', tanggalPublikasi = date(2020, 11, 11), author = 'hayasaka', abstract = 'lolololol', link = 'lol.com')
        response = Client().get('/studi/cari/', {'search':'lal'})
        html_response = response.content.decode('utf8')
        self.assertIn('Tidak Ditemukan', html_response)

    def test_study_add_post(self):
        response = Client().post('/studi/tambah/', {'namaStudi' : 'lol', 'tipeStudi' : 'Sosial', 'tanggalPublikasi' : date(2020, 11, 11), 'author' : 'hayasaka', 'abstract' : 'lolololol', 'link' : 'lol.com'})
        cnt = Study.objects.all().count()
        self.assertEqual(1, cnt)
    
