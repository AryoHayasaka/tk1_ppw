from django.urls import path
from .views import *

app_name = 'study'

urlpatterns = [
    path('tambah/', studyAddView, name='study'),
    path('cari/', studySearchView, name='searchstudy'),

]