from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('homepage.urls')),
    path('studi/', include('study.urls')),
    path('event/', include('event.urls')),
    path('support/', include('customer.urls'))
]
